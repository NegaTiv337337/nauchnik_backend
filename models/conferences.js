const Joi = require('joi');
const mongoose = require('mongoose');
const Common = require('../common.js');

const ConferenceSchema = new mongoose.Schema({
  ...Common.entity.mongoose,
  type: {
    type: String,
    required: true,
    enum: ['Международная', 'Всероссийская', 'Региональная'],
  },
  theme: {
    type: String,
    required: true,
    maxlength: 100,
  },
});

const Conference = mongoose.model('Conference', ConferenceSchema, 'nauchnik_conferences');
const schema = {
  ...Common.entity.joi,
  theme: Joi.string()
    .max(100)
    .required(),
  type: Joi.string()
    .valid(['Международная', 'Всероссийская', 'Региональная'])
    .required(),
};

//function to validate user
function validateConference(Conference) {
  return Joi.validate(Conference, schema);
}

exports.Conference = Conference;
exports.validate = validateConference;
exports.schema = schema;
