const jwt = require('jsonwebtoken');
const Joi = require('joi');
const uniqueValidator = require('mongoose-unique-validator');
const mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
  fio: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 50,
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true,
    index: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 255,
  },
  speciality: {
    type: String,
    required: false,
    maxlength: 50,
    default: ' ',
  },
  role: {
    type: String,
    enum: ['admin', 'manager', 'teacher'],
    required: true,
  },
});

//custom method to generate authToken
UserSchema.plugin(uniqueValidator);
UserSchema.methods.generateAuthToken = function() {
  const token = jwt.sign({_id: this._id, role: this.role, fio: this.fio}, process.env.PK); //get the private key from the config file -> environment variable
  return token;
};

const User = mongoose.model('User', UserSchema, 'nauchnik_users');
const schema = {
  fio: Joi.string()
    .min(2)
    .max(50)
    .required(),
  speciality: Joi.string().max(50),
  email: Joi.string()
    .min(5)
    .max(255)
    .required()
    .email(),
  password: Joi.string()
    .min(3)
    .max(255)
    .required(),
  role: Joi.string()
    .valid(['admin', 'manager', 'teacher'])
    .required(),
};

//function to validate user
function validateUser(user) {
  return Joi.validate(user, schema);
}

exports.User = User;
exports.validate = validateUser;
exports.schema = schema;
