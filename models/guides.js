const Joi = require('joi');
const mongoose = require('mongoose');
const Common = require('../common.js');

const GuideSchema = new mongoose.Schema({
  ...Common.entity.mongoose,
  info: {
    type: String,
    required: true,
    maxlength: 1000,
  },
  type: {
    type: String,
    required: true,
    enum: ['Учебное', 'Учебно-методическое', 'Методическое'],
  },
  isbn: {
    type: String,
    required: false,
    maxlength: 20,
  },
});

const Guide = mongoose.model('Guide', GuideSchema, 'nauchnik_guides');
const schema = {
  ...Common.entity.joi,
  isbn: Joi.string().max(20),
  type: Joi.string()
    .valid(['Учебное', 'Учебно-методическое', 'Методическое'])
    .required(),
};

//function to validate user
function validateGuide(Guide) {
  return Joi.validate(Guide, schema);
}

exports.Guide = Guide;
exports.validate = validateGuide;
exports.schema = schema;
