const Joi = require('joi');
const mongoose = require('mongoose');
const Common = require('../common.js');

const ArticleSchema = new mongoose.Schema({
  ...Common.entity.mongoose,
  info: {
    type: String,
    required: true,
    maxlength: 1000,
  },
  company: {
    type: String,
    required: true,
    enum: ['Scopus (WoS)', 'ВАК', 'РИНЦ', 'без БД'],
  },
  link: {
    type: String,
    required: false,
    maxlength: 500,
    default: ' ',
  },
});

const Article = mongoose.model('Article', ArticleSchema, 'nauchnik_articles');
const schema = {
  ...Common.entity.joi,
  link: Joi.string()
    // .uri()
    .max(500),
  company: Joi.string()
    .valid(['Scopus (WoS)', 'ВАК', 'РИНЦ', 'без БД'])
    .required(),
};

//function to validate user
function validateArticle(article) {
  return Joi.validate(article, schema);
}

exports.Article = Article;
exports.validate = validateArticle;
exports.schema = schema;
