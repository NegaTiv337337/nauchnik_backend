const express = require('express'),
  router = express.Router();
const bcrypt = require('bcrypt');

const Common = require('../common.js');
const userSchema = require('../models/user.js');

router.post('/', (req, res) => {
  let validationError = userSchema.validate(req.body).error;
  if (!validationError) {
    let pass = req.body.password;
    const hash = bcrypt.hashSync(pass, 8);
    req.body.password = hash;
    userSchema.User.create(req.body)
      .then((doc) => {
        logger.info('Added user: ', doc);
        return res.send(doc);
      })
      .catch(Common.mongooseCatch(res));
  } else {
    return Common.joiCatch(validationError, res);
  }
});

router.get('/', (req, res) => {
  let params = req.query.fio ? {fio: {$regex: `.*${req.query.fio}*.`, $options: 'i'}} : null;
  userSchema.User.find(params, {password: 0})
    .then((docs) => {
      logger.info('Users: ', docs);
      return res.send(docs);
    })
    .catch(Common.mongooseCatch(res));
});

router.put('/:id', (req, res) => {
  let usr = req.body;
  if (req.body.password) {
    let pass = req.body.password;
    const hash = bcrypt.hashSync(pass, 8);
    usr.password = hash;
  }
  userSchema.User.findOneAndUpdate({_id: req.params.id}, {$set: usr})
    .exec()
    .then(async (user) => {
      if (user) {
        console.log(user, user['fio']);
        // let validationError = userSchema.validate(user).error;
        // if (!validationError) {
        try {
          await user.save();
          logger.info('Edited user: ', user);
          return res.send(user);
        } catch (error) {
          logger.error(error);
          return res.send(error).status(500);
        }
        // } else {
        //   return Common.joiCatch(validationError, res);
        // }
      } else {
        logger.error('User not found: ', req.params.id);
        return res.sendStatus(404);
      }
    })
    .catch(Common.mongooseCatch(res));
});

router.delete('/:id', (req, res) => {
  userSchema.User.findByIdAndDelete(req.params.id)
    .then((user) => {
      if (user) {
        return res.sendStatus(200);
      } else {
        logger.error('User not found: ', req.params.id);
        return res.sendStatus(404);
      }
    })
    .catch(Common.mongooseCatch(res));
});

module.exports = router;
