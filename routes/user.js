var express = require('express'),
  router = express.Router();
const Joi = require('joi');
const DATA = require('../data.js');
const Common = require('../common.js');
const articleSchema = require('../models/articles.js');
const guideSchema = require('../models/guides.js');
const conferenceSchema = require('../models/conferences.js');

function validateRequest(req) {
  return Joi.validate(req, {
    articles: Joi.array().items(articleSchema.schema),
    conferences: Joi.array().items(conferenceSchema.schema),
    guides: Joi.array().items(guideSchema.schema),
  });
}

router.post('/data', (req, res) => {
  const data = req.body;
  let validationError = validateRequest(data).error;
  if (!validationError) {
    if (data.articles && data.articles.length > 0) {
      data.articles.forEach(el => {
        articleSchema.Article.create(el)
          .then(doc => {
            logger.info('Added article: ', doc);
          })
          .catch(Common.mongooseCatch(res));
      });
    }
    if (data.conferences && data.conferences.length > 0) {
      data.conferences.forEach(el => {
        conferenceSchema.Conference.create(el)
          .then(doc => {
            logger.info('Added conferences: ', doc);
          })
          .catch(Common.mongooseCatch(res));
      });
    }
    if (data.guides && data.guides.length > 0) {
      data.guides.forEach(el => {
        guideSchema.Guide.create(el)
          .then(doc => {
            logger.info('Added guides: ', doc);
          })
          .catch(Common.mongooseCatch(res));
      });
    }
    return res.sendStatus(200);
  } else {
    return Common.joiCatch(validationError, res);
  }
});

module.exports = router;
