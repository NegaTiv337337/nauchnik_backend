module.exports = {
  fio: 'Мусиенко Александр Викторович',
  datetime: '10.01.2020 04:20',
  currentYear: '2019-2020',
  aggregation: {
    articles: {
      items: [
        {
          company: 'РИНЦ',
          info: 'article info',
          link: '',
          year: '2034',
          academicYear: '2034-2035',
          coAuthors: [{name: 'Углин Георгий Ренатович'}],
        },
        {
          company: 'ВАК',
          info: 'article info',
          link: '',
          year: '2014',
          academicYear: '2014-2015',
          coAuthors: [{name: 'Углин Георгий Ренатович'}],
        },
        {
          company: 'без БД',
          info: 'article info',
          link: '',
          year: '2016',
          academicYear: '2016-2017',
          coAuthors: [{name: 'Углин Георгий Ренатович'}],
        },
      ],
    },
    guides: {
      items: [
        {
          type: 'Методическое',
          info: 'conference info',
          year: '2017',
          academicYear: '2016-2017',
          coAuthors: [{name: 'Абрамов Георгий Ренатович'}],
          isbn: '435354543',
        },
      ],
    },
    conferences: {
      items: [
        {
          type: 'Международная',
          info: 'conference info',
          year: '2017',
          academicYear: '2016-2017',
          coAuthors: [{name: 'Углин Георгий Ренатович'}],
          theme: 'Как танцевать тектоник',
        },
        {
          type: 'Региональная',
          info: 'conference info',
          year: '2016',
          academicYear: '2016-2017',
          coAuthors: [{name: 'Углин Георгий Ренатович'}],
          theme: 'Корелляция размера автомобиля и дохода человека',
        },
      ],
    },
  },
  people: [
    {
      fio: 'Толстыхин Владимир Андреевич',
      speciality: 'Младший сотрудник',
    },
    {
      fio: 'Казарцев Игорь Николаевич',
      speciality: 'Кандидат наук',
    },
  ],
};
