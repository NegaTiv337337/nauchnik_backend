require('dotenv').config();
const Sentry = require('@sentry/node');
Sentry.init({dsn: 'https://f498cb28dd934b59b90762998849c5b2@sentry.io/1805153'});
const logger = require('simple-node-logger').createSimpleLogger('main.log');
const fs = require('fs');
const path = require('path');
// logger.log = logger.info;
global.logger = logger;

const http = require('http');
const https = require('https');
const PizZip = require('pizzip');
const articleSchema = require('./models/articles.js');
const guideSchema = require('./models/guides.js');
const conferenceSchema = require('./models/conferences.js');
const Docxtemplater = require('docxtemplater');
const dateFns = require('date-fns');
const ru = require('date-fns/locale/ru');
const addYears = require('date-fns/addYears');
const docContent = fs.readFileSync(path.resolve(__dirname, './input.docx'), 'binary');
const mongoose = require('mongoose');
const cors = require('cors');
const errorhandler = require('errorhandler');
const Common = require('./common.js');
const jwtMiddleware = require('express-jwt-middleware');
const userRoutes = require('./routes/user.js');
const adminRoutes = require('./routes/admin.js');
const userSchema = require('./models/user.js');
const express = require('express');
const bcrypt = require('bcrypt');
const app = express();

//use config module to get the privatekey, if no private key set, end the application
if (!process.env.PK) {
  logger.error('FATAL ERROR: private key or/and salt is not defined.');
  process.exit(1);
}
const jwtCheck = jwtMiddleware(process.env.PK);
const adminCheck = (req, res, next) => {
  if (req.tokenData.role == 'admin') {
    next();
  } else {
    res.sendStatus(403);
  }
};

//connect to mongodb
mongoose
  .connect(process.env.MONGO_URL, {useNewUrlParser: true})
  .then(() => console.log('Connected to MongoDB...'))
  .catch((err) => console.error('Could not connect to MongoDB...'));

app.use(cors());
app.use(errorhandler({dumpExceptions: true, showStack: true}));
app.use(express.json());

app.get('/api/admin/report', async (req, res) => {
  const zip = new PizZip(docContent);
  const doc = new Docxtemplater();
  let results;
  try {
    results = await Promise.all([
      userSchema.User.find(),
      articleSchema.Article.find(),
      conferenceSchema.Conference.find(),
      guideSchema.Guide.find(),
    ]);
  } catch (err) {
    Common.mongooseCatch(res)(err);
  }
  const currentYear = `${dateFns.format(new Date(), 'yyyy', {locale: ru})}-${dateFns.format(
    addYears(new Date(), 1),
    'yyyy',
    {locale: ru},
  )}`;
  const datetime = dateFns.format(new Date(), 'dd.MM.yyyy HH:mm', {locale: ru});
  const data = {
    fio: 'Администратор',
    datetime,
    currentYear,
    aggregation: {
      articles:
        results[1].length > 0
          ? {
              items: results[1],
            }
          : null,
      conferences:
        results[2].length > 0
          ? {
              items: results[2],
            }
          : null,
      guides:
        results[3].length > 0
          ? {
              items: results[3],
            }
          : null,
    },
    people: results[0],
  };
  doc.loadZip(zip);
  doc.setData(data);
  try {
    doc.render();
  } catch (error) {
    var e = {
      message: error.message,
      name: error.name,
      stack: error.stack,
      properties: error.properties,
    };
    logger.error(JSON.stringify({error: e}));
    throw error;
  }
  const buf = doc.getZip().generate({type: 'nodebuffer'});
  try {
    fs.writeFileSync(path.resolve(__dirname, 'output2.docx'), buf);
  } catch (error) {
    var e = {
      message: error.message,
      name: error.name,
      stack: error.stack,
      properties: error.properties,
    };
    logger.error(JSON.stringify({error: e}));
    throw error;
  }
  return res.download(
    path.resolve(__dirname, 'output2.docx'),
    `Выполнение показателей на кафедре за ${currentYear} учебный год (${datetime}).docx`,
  );
});

app.use('/api/user', jwtCheck, userRoutes);
app.use('/api/admin', jwtCheck, adminCheck, adminRoutes);

app.post('/api/login', (req, res) => {
  userSchema.User.findOne({email: req.body.email})
    .then((user) => {
      if (user) {
        const isRight = bcrypt.compareSync(req.body.password, user.password);
        if (isRight) {
          const token = user.generateAuthToken();
          return res.send({user, token});
        } else {
          logger.error('Password not passed', req.body.password, req.body.email);
          return res.sendStatus(401);
        }
      } else {
        logger.error('User not found: ', req.query.email);
        return res.sendStatus(404);
      }
    })
    .catch(Common.mongooseCatch(res));
});

const port = process.env.PORT || 3000;

let privateKey, certificate, ca;

if (!process.env.DEV) {
  privateKey = fs.readFileSync('/etc/letsencrypt/live/veryweb.site/privkey.pem', 'utf8');
  certificate = fs.readFileSync('/etc/letsencrypt/live/veryweb.site/cert.pem', 'utf8');
  ca = fs.readFileSync('/etc/letsencrypt/live/veryweb.site/chain.pem', 'utf8');
}

const credentials = {
  key: privateKey,
  cert: certificate,
  ca: ca,
};

if (process.env.DEV) {
  const httpServer = http.createServer(app);
  httpServer.listen(port, () => {
    console.log('HTTP Server running on port: ' + port);
  });
} else {
  const httpsServer = https.createServer(credentials, app);
  httpsServer.listen(port, () => {
    console.log('HTTPS Server running on port: ' + port);
  });
}
