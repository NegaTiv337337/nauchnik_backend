const Joi = require('joi');
const mongoose = require('mongoose');
module.exports = {
  entity: {
    mongoose: {
      info: {
        type: String,
        required: false,
        maxlength: 1000,
      },
      year: {
        type: String,
        required: false,
      },
      academicYear: {
        type: String,
        required: false,
        maxlength: 9,
      },
      coAuthors: [{name: String}],
    },
    joi: {
      info: Joi.string().max(1000),
      year: Joi.string().max(4),
      academicYear: Joi.string().max(9),
      coAuthors: Joi.array().items(Joi.object({name: Joi.string()})),
    },
  },
  mongooseCatch: res => err => {
    logger.error(err);
    if (err.name === 'ValidationError' || err.name === 'CastError' || err.name === 'MongoError') {
      let msg = '';
      for (field in err.errors) {
        msg += err.errors[field].message + ' \n';
      }
      return res.status(400).send({error: 'Validation error.', details: msg});
    }
    return res.status(500).send({error: 'DB error: cant save doc.', details: err});
  },
  joiCatch: (validationError, res) => {
    logger.error(validationError);
    let msg = '';
    validationError.details.forEach(err => (msg += err.message + ' \n'));
    return res.status(400).send({error: 'Bad schema provided.', details: msg});
  },
};
